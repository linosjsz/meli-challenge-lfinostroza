package com.linostro.cleanarchitecturesample.framework

import com.linostro.data.ProductPersistenceSource
import com.linostro.domain.Product

class InMemoryProductPersistenceSource : ProductPersistenceSource {

    private var products: List<Product> = emptyList()

    override fun getPersistedProducts(): List<Product> = products

    override fun saveNewProduct(product: Product) {
        products = products + product
    }
}