package com.linostro.cleanarchitecturesample.framework


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.linostro.data.ProductSource
import com.linostro.domain.Product
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject


class ProductSource : ProductSource {

    var client: OkHttpClient = OkHttpClient()

    override  fun getProducts(inputText: String): Array<Product> {
        var items: Array<Product> = emptyArray()
        val url = "https://api.mercadolibre.com/products/search".toHttpUrlOrNull()?.newBuilder()
                ?.addQueryParameter("status", "active")
                ?.addQueryParameter("site_id", "MLA")
                ?.addQueryParameter("q",inputText)
                ?.build()
        val request = url?.let {
            okhttp3.Request.Builder()
                    .url(it)
                    .build()
        }

        client.newCall(request!!).execute().use {
            response ->
            val gson = Gson()
            var str_response = response.body!!.string()
            val json_objt = JSONObject(str_response)
            var json_results: JSONArray = json_objt.getJSONArray("results")

            val gsonB = GsonBuilder().create()
            val response_list = gsonB.fromJson(json_results.toString(),Array<Product>::class.java).toList()
            items = response_list.toTypedArray()

            return items
        }

        /*
        if (request != null) {
            client.newCall(request).enqueue(object : Callback {

                override fun onFailure(call: Call, e: IOException) {
                    Log.i("getProducts Error", e.toString())
                }

                override fun onResponse(call: Call, response: Response) {
                    val gson = Gson()
                    var str_response = response.body!!.string()
                    val json_objt = JSONObject(str_response)
                    var json_results:JSONArray = json_objt.getJSONArray("results")

                    val gsonB = GsonBuilder().create()
                    val response_list = gsonB.fromJson(json_results.toString(),Array<Product>::class.java).toList()
                   items = response_list.toTypedArray()

                }
            })
        }else{
            Log.i("getProducts Log", "request null")
        }
        */
        return items
    }

}