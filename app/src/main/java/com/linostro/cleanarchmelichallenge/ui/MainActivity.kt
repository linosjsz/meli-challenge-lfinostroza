package com.linostro.cleanarchitecturesample.ui

import android.os.Bundle
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.linostro.cleanarchitecturesample.R
import com.linostro.cleanarchitecturesample.framework.ProductSource
import com.linostro.cleanarchitecturesample.framework.InMemoryProductPersistenceSource
import com.linostro.data.ProductsRepository
import com.linostro.domain.Product
import com.linostro.usecases.GetProducts
import com.linostro.usecases.searchProduct
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainPresenter.View {

    private val productsAdapter = ProductsAdapter()
    private val presenter: MainPresenter
    private var inputText:String = ""
    private var btn_clicked = false

    init {
        val persistence = InMemoryProductPersistenceSource()
        val deviceProduct = ProductSource()
        val locationsRepository = ProductsRepository(persistence, deviceProduct)
        presenter = MainPresenter(
            this,
            GetProducts(locationsRepository),
            searchProduct(locationsRepository)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler.adapter = productsAdapter

        newProductBtn.setOnClickListener {
            if(!inputText.isNullOrBlank()) {
                btn_clicked = true
                presenter.searchProductClicked(inputText)
            }
            else{
                Toast.makeText(this, "Por favor ingresar un texto válido ", Toast.LENGTH_SHORT).show()
            }
        }
        search_product.onActionViewExpanded();
        search_product.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    inputText = newText
                }
                return false
            }
        })

        presenter.onCreate()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }


    override fun renderProducts(products: List<Product>) {
        if(products.size > 0){
            productsAdapter.items = products
        }
        else if(products.size <= 0 && btn_clicked){
            productsAdapter.items = emptyList()
            Toast.makeText(this, "No se Encontraron productos", Toast.LENGTH_SHORT).show()
        }
    }
}