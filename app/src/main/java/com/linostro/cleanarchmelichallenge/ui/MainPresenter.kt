package com.linostro.cleanarchitecturesample.ui

import android.util.Log
import com.linostro.domain.Product as DomainLocation
import com.linostro.usecases.GetProducts
import com.linostro.usecases.searchProduct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainPresenter(
        private var view: View?,
        private val getProducts: GetProducts,
        private val searchProduct: searchProduct
) {
    interface View {
        fun renderProducts(products: List<com.linostro.domain.Product>)
    }

    fun onCreate() = GlobalScope.launch(Dispatchers.Main) {
        val products = withContext(Dispatchers.IO) { getProducts() }
        view?.renderProducts(products.map(DomainLocation::toPresentationModel))
    }

    fun searchProductClicked(inputText: String) = GlobalScope.launch(Dispatchers.Main) {
        val products = withContext(Dispatchers.IO) { searchProduct(inputText) }
        Log.i("clicked prodcut", "products :"+ products)
        view?.renderProducts(products.map(DomainLocation::toPresentationModel))
    }

    fun onDestroy() {
        view = null
    }
}