package com.linostro.cleanarchitecturesample.ui

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import com.linostro.domain.Product as DomainLocation

data class Product(val name: String, val id: String)

fun newProductClicked() = GlobalScope.launch(Dispatchers.Main) {
    Log.i("clicked prodcut","product clicked")
}


fun DomainLocation.toPresentationModel(): com.linostro.domain.Product = com.linostro.domain.Product(
        name,id
)

