package com.linostro.cleanarchitecturesample.ui

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.linostro.cleanarchitecturesample.R
import com.linostro.domain.Product
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product_item.*
import kotlin.properties.Delegates

class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    var items: List<Product> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.view_product_item))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        @SuppressLint("SetTextI18n")
        fun bind(product: Product) {
            with(product) {
                productName.text = product.name
                productDesc.text = product.id
            }
        }
    }
}
