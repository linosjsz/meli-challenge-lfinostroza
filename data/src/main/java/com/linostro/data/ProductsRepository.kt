package com.linostro.data
import com.linostro.domain.Product

class ProductsRepository(
    private val productPersistenceSource: ProductPersistenceSource,
    private val productSource: ProductSource
) {

    fun getSavedProducts(): List<Product> = productPersistenceSource.getPersistedProducts()

    fun requestNewProduct(inputText: String): List<Product> {
        val newProduct = productSource.getProducts(inputText)
        return  newProduct.toList()
        //productPersistenceSource.saveNewProduct(newProduct)
        //return getSavedProducts()
    }

}

interface ProductPersistenceSource {

    fun getPersistedProducts(): List<Product>
    fun saveNewProduct(product: Product)

}

interface ProductSource {

    fun getProducts(inputText: String): Array<Product>

}