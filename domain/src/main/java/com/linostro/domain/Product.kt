package com.linostro.domain

import com.google.gson.annotations.SerializedName;
import kotlin.collections.arrayListOf as arrayListOf1

data class Product(
        @SerializedName("name"          ) var name         : String?               = null,
        @SerializedName("id"            ) var id           : String?               = null,
        @SerializedName("status"        ) var status       : String?               = null,
        @SerializedName("domain_id"     ) var domainId     : String?               = null,
        @SerializedName("settings"      ) var settings     : Settings?             = Settings(),
        @SerializedName("main_features" ) var mainFeatures : ArrayList<String>     = arrayListOf1(),
        @SerializedName("attributes"    ) var attributes   : ArrayList<Attributes> = arrayListOf1(),
        @SerializedName("pictures"      ) var pictures     : ArrayList<Pictures>   = arrayListOf1(),
        @SerializedName("parent_id"     ) var parentId     : String?               = null,
        @SerializedName("children_ids"  ) var childrenIds  : ArrayList<String>     = arrayListOf1()
        )

data class Attributes (

        @SerializedName("id"         ) var id        : String? = null,
        @SerializedName("name"       ) var name      : String? = null,
        @SerializedName("value_id"   ) var valueId   : String? = null,
        @SerializedName("value_name" ) var valueName : String? = null

)


data class Pictures (

        @SerializedName("id"  ) var id  : String? = null,
        @SerializedName("url" ) var url : String? = null

)

data class Settings (

        @SerializedName("listing_strategy" ) var listingStrategy : String? = null

)