package com.linostro.usecases

import com.linostro.data.ProductsRepository
import com.linostro.domain.Product

class GetProducts(private val productsRepository: ProductsRepository) {

    operator fun invoke(): List<Product> = productsRepository.getSavedProducts()

}
