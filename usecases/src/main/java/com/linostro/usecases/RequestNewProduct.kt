package com.linostro.usecases


import com.linostro.data.ProductsRepository
import com.linostro.domain.Product

class searchProduct(private val productsRepository: ProductsRepository) {

    operator fun invoke(inputText: String): List<Product> = productsRepository.requestNewProduct(inputText)

}
